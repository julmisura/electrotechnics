void setup()
{
  pinMode(13, OUTPUT);
}

void loop()
{
  letterA();
  letterB();
  letterC();
  letterD();
  letterE();
  letterF();
  letterG();
  letterH();
  letterI();
  letterJ();
  offWord();
  blink30Hz();
  delay(5000);
}

int oneUnit = 250; //milliseconds

void onShort()
{
  digitalWrite(13, HIGH);
  delay(oneUnit);
}

void onLong()
{
  digitalWrite(13, HIGH);
  delay(3 * oneUnit);
}

void offPause() //between parts of letters
{
  digitalWrite(13, LOW);
  delay(oneUnit); 
}

void offLetter()
{
  digitalWrite(13, LOW);
  delay(3 * oneUnit);
}

void offWord()
{
  digitalWrite(13, LOW);
  delay(7 * oneUnit);
}

void letterA()
{
  onShort();
  offPause();
  onLong();
  offLetter();
}

void letterB()
{
  onLong();
  offPause();
  onShort();
  offPause();
  onShort();
  offPause();
  onShort();
  offLetter();
}

void letterC() 
{
  onLong();
  offPause();
  onShort();
  offPause();
  onLong();
  offPause();
  onShort();
  offLetter();
}

void letterD()
{
  onLong();
  offPause();
  onShort();
  offPause();
  onShort();
  offLetter();
}

void letterE()
{
  onShort();
  offLetter();
}

void letterF()
{
  onShort();
  offPause();
  onShort();
  offPause();
  onLong();
  offPause();
  onShort();
  offLetter();
}

void letterG()
{
  onLong();
  offPause();
  onLong();
  offPause();
  onShort();
  offLetter();
}

void letterH()
{
  onShort();
  offPause();
  onShort();
  offPause();
  onShort();
  offPause();
  onShort();
  offLetter();
}

void letterI()
{
  onShort();
  offPause();
  onShort();
  offLetter();
}

void letterJ()
{
  onShort();
  offPause();
  onLong();
  offPause();
  onLong();
  offPause();
  onLong();
  offLetter();
}

void blink30Hz()
{
  for (int i = 0; i < 150; i++) {
    digitalWrite(13, HIGH);
    delay(17);
    digitalWrite(13, LOW);
    delay(17);
  }
}